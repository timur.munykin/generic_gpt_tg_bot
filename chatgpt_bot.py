import os
import openai
from telegram import Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext
from telegram import Bot, Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext

openai.api_key = os.getenv("OPENAI_API_KEY")
TELEGRAM_BOT_TOKEN = os.getenv("TG_API_KEY")

zones = [
    {
        "lat_min": 50.121356,
        "lat_max": 50.121503,
        "lon_min": 14.454124,
        "lon_max": 14.454744,
        "image": "./images/2.jpg"
    },
    {
        "lat_min": 50.120175,
        "lat_max": 50.120716,
        "lon_min": 14.455242,
        "lon_max": 14.455531,
        "image": "./images/4.jpg"
    },
    {
        "lat_min": 50.1036227,
        "lat_max": 50.1051797,
        "lon_min": 14.4661149,
        "lon_max": 14.4688072,
        "image": "./images/1.jpg"
    },
    {
        "lat_min": 50.0957229,
        "lat_max": 50.0958859,
        "lon_min": 14.4511455,
        "lon_max": 14.4536945,
        "image": "./images/5.jpg"
    },
    {
        "lat_min": 50.0933474,
        "lat_max": 50.0937308,
        "lon_min": 14.4459880,
        "lon_max": 14.448205198419151,
        "image": "./images/11.jpg"
    },
    {
        "lat_min": 50.092132955715684,
        "lat_max": 50.09282027378272,
        "lon_min": 14.446996636831653,
        "lon_max": 14.448397277098099,
        "image": "./images/3.jpg"
    },
    {
        "lat_min": 50.092940,
        "lat_max": 50.092940,
        "lon_min": 14.446297,
        "lon_max": 14.449191,
        "image": "./images/3.jpg"
    },
    {
        "lat_min": 50.08733480135451,
        "lat_max": 50.088706874146744,
        "lon_min": 14.452456604651623,
        "lon_max": 14.455009139497523,
        "image": "./images/9.jpg"
    },
    {
        "lat_min": 50.087036670000565,
        "lat_max": 50.087771116404106,
        "lon_min": 14.419477041345694,
        "lon_max": 14.422484981483489,
        "image": "./images/6.jpg"
    },
    {
        "lat_min": 50.041411,
        "lat_max": 50.044263,
        "lon_min": 14.411478,
        "lon_max": 14.413093,
        "image": "./images/10.jpg"
    },
    {
        "lat_min": 50.08626459156675,
        "lat_max": 50.08820089189083,
        "lon_min": 14.426890010849723,
        "lon_max": 14.430237102172523,
        "image": "./images/12.jpg"
    },
    {
        "lat_min": 50.094501,
        "lat_max": 50.100623,
        "lon_min": 14.420912,
        "lon_max": 14.431000,
        "image": "./images/7.jpg"
    },
    {
        "lat_min": 50.118336548965935,
        "lat_max": 50.122495361303734,
        "lon_min": 14.412123773635503,
        "lon_max": 14.414331310140149,
        "image": "./images/13.jpg"
    }



    # ... добавьте другие зоны по аналогии
]

def get_image_for_location(latitude, longitude):
    for zone in zones:
        if zone["lat_min"] < latitude < zone["lat_max"] and zone["lon_min"] < longitude < zone["lon_max"]:
            return zone["image"]
    return None

# def get_image_for_location(latitude, longitude):
#     # Эти значения являются просто примерами и предназначены для демонстрации.
#     # На практике вы можете использовать более сложные алгоритмы или структуры данных.
#     if 50.121200 < latitude < 50.142000 and 14.451000 < longitude < 14.459000:
#         return "./images/example.jpg"
#     elif 56.0 < latitude < 57.0 and 38.0 < longitude < 39.0:
#         return "./images/example2.jpg"
#     else:
#         return None

def handle_text(update: Update, context: CallbackContext) -> None:
    user_message = update.message.text
    update.message.reply_text(f"Вы отправили: {user_message}")

def handle_location(update: Update, context: CallbackContext) -> None:
    user_location = update.message.location
    image_path = get_image_for_location(user_location.latitude, user_location.longitude)

    if image_path:
        bot = update.message.bot
        bot.send_photo(chat_id=update.message.chat_id, photo=open(image_path, 'rb'))
    else:
        update.message.reply_text(f"К сожалению, у меня нет изображений для этих координат.{user_location.latitude, user_location.longitude}")

def main():
    updater = Updater(token=TELEGRAM_BOT_TOKEN)

    dp = updater.dispatcher

    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, handle_text))
    dp.add_handler(MessageHandler(Filters.location, handle_location))

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()

# # print(os.getcwd())
# x = open("./images/example.jpg", 'rb')
# print(x)
